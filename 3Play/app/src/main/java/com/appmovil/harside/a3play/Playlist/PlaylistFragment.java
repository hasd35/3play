package com.appmovil.harside.a3play.Playlist;


import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.appmovil.harside.a3play.PlayerService.PlayerService;
import com.appmovil.harside.a3play.PlayerService.PlayerService.PlayerBinder;
import com.appmovil.harside.a3play.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaylistFragment extends Fragment implements View.OnClickListener {

    View mView;
    Context context;
    ArrayList<Song> songList;
    SwipeRefreshLayout srfRvContainer;
    ToggleButton btnPlayPause;
    ListView listLV;
    TextView tvSongTimeFragment;
    TextView tvCurrentTime;

    int songPos;

    SeekBar seekBar;

    //Player Variables

    private PlayerService musicSrv;
    private Intent playIntent;
    private boolean musicBound = false;
    private int currentTime;


    public PlaylistFragment() {
        // Required empty public constructor
    }

    private ServiceConnection musicConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PlayerBinder binder = (PlayerBinder) service;
            //get service
            musicSrv = binder.getService();
            //pass list
            musicSrv.setList(songList);
            musicBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            musicBound = false;
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onStart() {
        super.onStart();
        if (playIntent == null) {
            playIntent = new Intent(context, PlayerService.class);
            context.bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            context.startService(playIntent);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mView = inflater.inflate(R.layout.fragment_playlist, container, false);
        context = mView.getContext();

        btnPlayPause = mView.findViewById(R.id.btnPlayPause);
        srfRvContainer = mView.findViewById(R.id.srlContainer);
        tvSongTimeFragment = mView.findViewById(R.id.tvSongTimeFragment);
        tvCurrentTime = mView.findViewById(R.id.tvCurrentTimeFragment);
        seekBar = mView.findViewById(R.id.seekBar);

        mView.findViewById(R.id.btnForward).setOnClickListener(this);
        mView.findViewById(R.id.btnBackward).setOnClickListener(this);


        btnPlayPause.setChecked(true);
        btnPlayPause.setOnClickListener(this);
        songList = new ArrayList<>();
        try {
            getMusic();
        } catch (IOException e) {
            e.printStackTrace();
        }


        srfRvContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    getMusic();
                    srfRvContainer.setRefreshing(false);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        //method by sort list songs alphabetically
        Collections.sort(songList, new Comparator<Song>() {
            public int compare(Song a, Song b) {

                return a.getTitle().compareTo(b.getTitle());
            }
        });


        listLV = mView.findViewById(R.id.list);
        listLV.setAdapter(new SongAdapter(mView.getContext(), songList));

        listLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(view.getContext(), "Canción:" + songList.get(position).getTitle(), Toast.LENGTH_SHORT).show();
                btnPlayPause.setChecked(false);
                songPos = position;
                tvSongTimeFragment.setText(getSongTimeMin(songList.get(position).getDuration()));
                seekBar.setProgress(0);
                seekBar.setMax(songList.get(position).getDuration());
                musicSrv.setSong(position);
                try {
                    musicSrv.playSong();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                currentTime = musicSrv.currentTime();

                seekBar.setProgress(currentTime);


                    System.out.println("currentTime:" + musicSrv.currentTime());

                seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        tvCurrentTime.setText(getSongTimeMin(progress));
                        musicSrv.setCurrentTime(progress);
                        try {
                            musicSrv.restartSong();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
            }
        });


        Log.i("CurrentTime", "Time:" + currentTime);


        return mView;

    }

    public void getMusic() throws IOException {
        ContentResolver contentResolver = mView.getContext().getContentResolver();
        Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        File music = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);

        //Uri musicUri = Uri.fromFile(music);
        Cursor songCursor = contentResolver.query(musicUri, null, null, null, null);

        System.out.println("URI Music:" + musicUri);
        if (songCursor != null && songCursor.moveToFirst()) {
            int songID = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int songTitle = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int songArtist = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int songDuration = songCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
            int songFileName = songCursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME);
            songList.removeAll(songList);
            do {
                Song currentSong = new Song();
                int currentID = songCursor.getInt(songID);
                String currentTittle = songCursor.getString(songTitle);
                String currentArtist = songCursor.getString(songArtist);
                int currentDuration = songCursor.getInt(songDuration);
                String currentFileName = songCursor.getString(songFileName);


                Log.i("getMusic", "currentTittle=" + currentID);
                currentSong.set_ID(currentID);
                currentSong.setTitle(currentTittle);
                currentSong.setArtist(currentArtist);
                currentSong.setDuration(currentDuration);
                currentSong.setFileName(currentFileName);

                songList.add(currentSong);

            } while (songCursor.moveToNext());

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPlayPause:
                //Toast.makeText(context, "Has pulsado el boton play/pause", Toast.LENGTH_SHORT).show();

                Log.i("SongPos", "SongPos:" + songPos);
                //musicSrv.setSong(songPos);
                if (btnPlayPause.isChecked()) {
                    musicSrv.pauseSong();
                    currentTime = musicSrv.currentTime();
                } else {
                    try {
                        musicSrv.restartSong();
                        seekBar.setProgress(currentTime);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                break;
            case R.id.btnForward:
                //Toast.makeText(context, "Has pulsado el boton btnForward", Toast.LENGTH_SHORT).show();

                if (songPos == songList.size() - 1) {
                    songPos = 0;
                } else {
                    songPos += 1;
                }

                musicSrv.setSong(songPos);
                try {
                    musicSrv.playSong();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                break;
            case R.id.btnBackward:
                Toast.makeText(context, "Has pulsado el boton btnBackward", Toast.LENGTH_SHORT).show();
                if (songPos == 0) {
                    songPos = songList.size() - 1;
                } else {
                    songPos -= 1;

                }
                musicSrv.setSong(songPos);
                try {
                    musicSrv.playSong();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btnShuffle:
                Toast.makeText(context, "Has pulsado el boton btnShuffle", Toast.LENGTH_SHORT).show();

                break;
            case R.id.btnRandom:
                Toast.makeText(context, "Has pulsado el boton btnRandom", Toast.LENGTH_SHORT).show();

                break;

        }
        Toast.makeText(context, songList.get(songPos).getTitle(), Toast.LENGTH_SHORT).show();

    }

    @SuppressLint("DefaultLocale")
    public String getSongTimeMin(int minutes) {

        long min = TimeUnit.MILLISECONDS.toMinutes(minutes);
        long sec = TimeUnit.MILLISECONDS.toSeconds(minutes) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(minutes));

        String time = "00:00";
        if (min < 10) {
            time = "0" + String.valueOf(min);
        } else {
            time = String.valueOf(min);
        }
        if (sec < 10) {
            time = time + ":0" + String.valueOf(sec);
        } else {
            time = time + ":" + String.valueOf(sec);

        }


        return time;
    }
}
