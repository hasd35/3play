package com.appmovil.harside.a3play;

import android.*;
import android.app.ActionBar;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.appmovil.harside.a3play.Player.PlayerFragment;
import com.appmovil.harside.a3play.Playlist.PlaylistFragment;
import com.appmovil.harside.a3play.Playlist.Song;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    PlaylistFragment playlistFragment;
    PlayerFragment playerFragment;
    FragmentTransaction ft;

    ArrayList<Song> arrayList;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            ft = getSupportFragmentManager().beginTransaction();

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    ft.replace(R.id.flMain,playlistFragment).commit();

                    return true;
                case R.id.navigation_dashboard:
                    ft.replace(R.id.flMain,playerFragment).commit();


                    return true;
                }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.title_bar);

        playlistFragment = new PlaylistFragment();
        playerFragment = new PlayerFragment();

        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.flMain,playlistFragment).commit();

        Intent recIntent = getIntent();
        if(recIntent.getBooleanExtra("cambioTab",false)){
            Toast.makeText(this, "Change Tab", Toast.LENGTH_SHORT).show();
            ArrayList<Song> songList= recIntent.getParcelableArrayListExtra("listSong");
            int currentPosition = recIntent.getIntExtra("currentPosition",0);
            playerFragment.setMusic(songList);
        }
        


        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Sin permiso de lectura", Toast.LENGTH_SHORT).show();

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this, "Es necesario explicarle al usuario por que se necesita el permiso", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Solicitando permiso", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        100);
            }
        }
    }







    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d("MainActivity", "onRequestPermissionsResult() called with: requestCode = [" + requestCode + "], permissions = [" + permissions + "], grantResults = [" + grantResults + "]");
        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Ya se tiene permiso de lectura", Toast.LENGTH_SHORT).show();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(this, "El usuario rechazo la solicitud", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    boolean activePlayerFragment=false;
    public void changeTab(boolean state){
        activePlayerFragment = state;

    }


}
