package com.appmovil.harside.a3play.Playlist;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appmovil.harside.a3play.R;

import java.text.DateFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SongAdapter extends ArrayAdapter<Song> {
    private List<Song> songList;
    private int[] itemsColors;


    public SongAdapter(@NonNull Context context, @NonNull List<Song> objects) {
        super(context, -1, objects);
        this.songList = objects;
        itemsColors = context.getResources().getIntArray(R.array.items_colors);
    }


    @SuppressLint("DefaultLocale")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View mView = inflater.inflate(R.layout.playlist_row, parent, false);

        ImageView iconIV = mView.findViewById(R.id.item_icon);
        ImageView separatorPointIV = mView.findViewById(R.id.separator_point);
        TextView titleTV = mView.findViewById(R.id.item_title);
        TextView artistTV = mView.findViewById(R.id.item_description);
        TextView durTV = mView.findViewById(R.id.item_dur);
        TextView minTV = mView.findViewById(R.id.item_min);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int itemColorIndex = position % itemsColors.length;
            separatorPointIV.setBackgroundTintList(ColorStateList.valueOf(itemsColors[itemColorIndex]));
        }

        iconIV.setImageDrawable(ContextCompat.getDrawable(mView.getContext(),R.drawable.ic_disk));
        titleTV.setText(songList.get(position).getTitle());
        artistTV.setText(songList.get(position).getArtist());

        DateFormat dateFormat = DateFormat.getDateInstance();

        int minutes = songList.get(position).getDuration();
        String min= String.format("%d:%d",
                TimeUnit.MILLISECONDS.toMinutes(minutes),
                TimeUnit.MILLISECONDS.toSeconds(minutes) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(minutes))
        );
        durTV.setText(min);
        minTV.setText("min");

        mView.setTag(position);

        return mView;

    }
}
