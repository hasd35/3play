package com.appmovil.harside.a3play.Player;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.appmovil.harside.a3play.MainActivity;
import com.appmovil.harside.a3play.Playlist.Song;
import com.appmovil.harside.a3play.Playlist.SongAdapter;
import com.appmovil.harside.a3play.R;

import java.io.IOException;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlayerFragment extends Fragment {
    ArrayList<Song> songList;
    SwipeRefreshLayout srfRvContainer;
    View mView;
    public PlayerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_player, container, false);

        songList = new ArrayList<>();
        ListView listLV = mView.findViewById(R.id.listPLayer);
        listLV.setAdapter(new SongAdapter(mView.getContext(),songList));

        listLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(view.getContext(), "Canción:"+songList.get(position).getTitle(), Toast.LENGTH_SHORT).show();

            }
        });
        return mView;
    }

    public void setMusic(ArrayList<Song> songList){
        this.songList = songList;
    }
}
