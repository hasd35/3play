package com.appmovil.harside.a3play.PlayerService;

import android.app.IntentService;
import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.appmovil.harside.a3play.Playlist.Song;

import java.io.IOException;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class PlayerService extends Service implements
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener {

    //media player
    private MediaPlayer player;
    //song list
    private ArrayList<Song> listSong;
    //current song position
    private int currentSongPos;

    private int currentTime;

    private final IBinder playerBind = new PlayerBinder();

    Boolean isPause = false;

    public PlayerService() {
    }


    public void playSong() throws IOException {
        //play a song method
        player.reset();
        //get song
        isPause = false;
        Song playSong = listSong.get(currentSongPos);
        //get id
        long currSong = playSong.get_ID();
        //set uri

        Uri trackUri = ContentUris.withAppendedId(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, currSong);

        try {
            player.setDataSource(getApplicationContext(), trackUri);
            if (isPause) {
                Log.i(TAG, "CurrentTime in Service:" + currentTime);
                player.seekTo(currentTime);
            }
        } catch (Exception e) {
            Log.e("MUSIC SERVICE", "Error setting data source", e);
        }
        player.prepare();
    }

    public void pauseSong() {
        //play a song method
        player.pause();
        currentTime = player.getCurrentPosition();
        Log.i("CurrentTimeTAG", "Time Service Song:" + currentTime);
        isPause = true;
        //player.prepareAsync();
    }

    public void restartSong() throws IOException {
        player.seekTo(currentTime);
        player.start();

    }

    public int currentTime() {
        return player.getCurrentPosition();
    }
    public void setCurrentTime(int currentTime){
        this.currentTime=currentTime;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return playerBind;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        player.stop();
        player.release();
        return false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        currentSongPos = 0;
        currentTime = 0;
        player = new MediaPlayer();
        initMusicPlayer();

    }

    public void initMusicPlayer() {
        //set player properties
        player.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setOnPreparedListener(this);
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
    }

    public void setList(ArrayList<Song> listSong) {
        this.listSong = listSong;
    }


    public void setSong(int songIndex) {
        currentSongPos = songIndex;
    }


    public class PlayerBinder extends Binder {


        public PlayerService getService() {
            return PlayerService.this;
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

        if(currentSongPos==listSong.size()-1){
            player.stop();
            Toast.makeText(this, "Fin de lista ", Toast.LENGTH_SHORT).show();

        }else {
            currentSongPos++;
            try {

                playSong();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }
}
