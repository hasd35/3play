package com.appmovil.harside.a3play.Playlist;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Song implements Parcelable{
    private int _ID;
    private String title;
    private String artist;
    private int duration;
    private String fileName;
    private int icon;

    public Song() {
    }

    public Song(int _ID, String title, String artist, int duration, String fileName, int icon) {
        this._ID = _ID;
        this.title = title;
        this.artist = artist;
        this.duration = duration;
        this.fileName = fileName;
        this.icon = icon;
    }

    public Song(Parcel in) {
        this._ID =  in.readInt();
        this.title = in.readString();
        this.artist = in.readString();
        this.duration = in.readInt();
        this.fileName = in.readString();
        this.icon = in.readInt();

    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    public void set_ID(int _ID) {
        this._ID=_ID;
    }

    public int get_ID() {
        return _ID;
    }

    @Override
    public String toString() {
        return "Song{\n" +
                "_ID=" + _ID +
                ", \ntitle='" + title + '\'' +
                ", \nartist='" + artist + '\'' +
                ", \nduration=" + duration +
                ", \nfileName='" + fileName + '\'' +
                ", \nicon=" + icon +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_ID);
        dest.writeString(title);
        dest.writeString(artist);
        dest.writeInt(duration);
        dest.writeString(fileName);
        dest.writeInt(icon);

    }

    public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>(){
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

}

